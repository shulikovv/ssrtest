import express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import App from './client/App';
import Profile from './client/Profile';
import Html from './client/Html';
import { ServerStyleSheet } from 'styled-components'; // <-- importing ServerStyleSheet

import passport from 'passport';
import { Strategy } from 'passport-facebook';

import expressSession from 'express-session';

import User from './models/user';

const port = 3000;
const server = express();


server.use(expressSession({secret: 'mySecretKey'}));
server.use(passport.initialize());
server.use(passport.session());

// Initialize Passport
import initPassport from './passport/init';
initPassport(passport);


// Creating a single index route to server our React application from.
server.get('/', (req, res) => {
  let body;
  const title = 'Server side Rendering with fb login';
  if (req.isAuthenticated()) {
    const user = req.user;
    const account = req.account;
    User.findOne({where:{profile_id:user.id}}).then(currentUser=>{
      const _sheet = new ServerStyleSheet();
      body = renderToString(_sheet.collectStyles(<Profile user={currentUser} />));
      res.send(
        Html({
          body,
          styles: _sheet.getStyleTags(),
          title
        })
      );
    })
  } else {
    const sheet = new ServerStyleSheet();
    const styles = sheet.getStyleTags();
    body = renderToString(sheet.collectStyles(<App />));
    res.send(
      Html({
        body,
        styles,
        title
      })
    );

  }

});

server.get('/login/facebook',
  passport.authenticate('facebook'));

server.get('/login/facebook/return',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });

server.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

server.listen(port);
console.log(`Serving at http://localhost:${port}`);