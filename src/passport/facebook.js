import FacebookStrategy, {Strategy} from 'passport-facebook';
import User from '../models/user';
import fbConfig from '../fb.js';

export default function(passport) {

    passport.use('facebook', new FacebookStrategy({
        clientID        : fbConfig.appID,
        clientSecret    : fbConfig.appSecret,
        callbackURL     : fbConfig.callbackUrl
    },

    // facebook will send back the tokens and profile
    function(access_token, refresh_token, profile, done) {

    	console.log('profile', profile);

		// asynchronous
		process.nextTick(function() {

			// find the user in the database based on their facebook id
	        User.findOne({where:{ 'profile_id' : profile.id }}).then((user) => {
				// if the user is found, then log them in
	            if (user) {
	                return done(null, user); // user found, return that user
	            } else {
	                // if there is no user found with that facebook id, create them
					User.create({
						profile_id:profile.id, // set the users facebook id
						access_token: access_token, // we will save the token that facebook provides to the user
						firstName: profile.name.givenName,
						lastName: profile.name.familyName, // look at the passport user profile to see how names are returned
						email: Array.isArray(profile.emails)?profile.emails[0].value:'', // facebook can return multiple emails so we'll take the first
						picture: Array.isArray(profile.photos)?profile.photos[0].value:'', // facebook can return multiple emails so we'll take the first
						userName: '',
					}).then(user=>{
						return done(null, user);
					})
	            }

	        }).catch(err=>done(err));
        });

    }));

};
