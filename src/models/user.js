import config from '../config/config.json';
import Sequelize from 'sequelize';
const sequelize = new Sequelize(config.development.database, config.development.username, config.development.password, {
  host: config.development.host,
  dialect: config.development.dialect,
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

});

export default sequelize.define('user', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true,
	},
	profile_id: {
		type: Sequelize.STRING
	},
	access_token: {
		type: Sequelize.STRING
	},
	firstName: {
		type: Sequelize.STRING
	},
	lastName: {
		type: Sequelize.STRING
	},
	email: {
		type: Sequelize.STRING
	},
	userName: {
		type: Sequelize.STRING
	},
	picture: {
		type: Sequelize.STRING
	},
	createdAt: Sequelize.DATE,
	updatedAt: Sequelize.DATE,
});