import React from 'react';
import styled from 'styled-components';
const AppContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  width: 100%;
  height: 100%;
  font-size: 40px;
  background: linear-gradient(20deg, rgb(219, 112, 147), #daa357);
`;

const Container2 = styled.div`
  display: flex;
  flex-direction: column;
`;

class App extends React.Component {
  render() {
    let {user} = this.props;
    let {userName, firstName, lastName, access_token, picture} = user;
    return (
      <AppContainer>
        <Container2>
          <div><img src={picture} /></div>
          <div>{`${firstName} ${lastName}`}</div>
          <div><a href="/logout">Logout</a></div>
        </Container2>
      </AppContainer>
    )
  }
}


export default App;