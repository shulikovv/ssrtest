// facebook app settings
export default {
	'appID' : '<your_app_id>',
	'appSecret' : '<your_app_secret>',
	'callbackUrl' : 'http://localhost:3000/login/facebook/callback'
}